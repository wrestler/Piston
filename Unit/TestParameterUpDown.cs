﻿using NUnit.Framework;
using Piston;

namespace Unit
{
    /// <summary>
    /// Тест класса пользовательского контрола параметра
    /// </summary>
    [TestFixture]
    public class TestParameterUpDown
    {
        /// <summary>
        /// Тест класса ParameterUpDown
        /// </summary>
        /// <param name="parameterType"> тип параметра модели</param>
        /// <param name="newValue"> новое значение</param>
        /// <returns></returns>
        [TestCase(ParameterType.HeadDiameter, 60.0, Result = 60.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в валидное значение")]
        [TestCase(ParameterType.HeadDiameter, 20.0, Result = 25.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в минимальное значение")]
        [TestCase(ParameterType.HeadDiameter, 150.0, Result = 100.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в максимальное значение")]
        public double SetParameterUpDownWithMaxValue(ParameterType parameterType, double newValue)
        {
            //arrange
            var pistonProperties = new PistonProperties();
            var parameterUpDown = new ParameterUpDown {Parameter = pistonProperties.GetParameter(parameterType)};

            //act
            parameterUpDown.Parameter.Value = newValue;

            //assert
            return parameterUpDown.Parameter.Value;
        }
    }
} 
