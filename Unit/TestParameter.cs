﻿using System.Runtime.InteropServices.ComTypes;
using NUnit.Framework;
using Piston;

namespace Unit
{
    /// <summary>
    /// Тест класса параметра
    /// </summary>
    [TestFixture]
    public class TestParameter
    {
        /// <summary>
        /// Тест класса Parameter
        /// </summary>
        /// <param name="initValue"> текущее валидное значение</param>
        /// <param name="newValue"> новое значение для проверки на валидность</param>
        /// <param name="minValue"> минимально возможное значеие</param>
        /// <param name="maxValue"> макмимально возможное значение</param>
        /// <param name="expectedValue"> значение, которое должны получить после валидации</param>
        [TestCase(8.0, 10.0, 5.0, 15.0, 10, 
            Description = "Установка параметра в валидное значение")]
        [TestCase(8.0, 20.0, 5.0, 15.0, 15.0,
            Description = "Установка параметра в максимальное значение")]
        [TestCase(8.0, 1.0, 5.0, 15.0, 5.0,
            Description = "Установка параметра в максимальное значение")]
        public void SetParameter(double initValue, double newValue, double minValue, double maxValue, double expectedValue)
        {
            //arrange and act
            var parameter = new Parameter(minValue, initValue, maxValue) {Value = newValue};

            //assert
            Assert.AreEqual(expectedValue, parameter.Value, 0.0, "Value is incorrect!");
        }
    }
}
