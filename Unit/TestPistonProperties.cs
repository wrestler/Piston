﻿using NUnit.Framework;
using Piston;

namespace Unit
{
   /// <summary>
    /// Тест класса пользовательского кОнтрола параметра
   /// </summary>
    [TestFixture]
    public class TestPistonProperties
    {
        /// <summary>
        /// Тест класса PistonProperties
        /// </summary>
        /// <param name="parameterType"> тип параметра модели</param>
        /// <param name="newValue"> новое значение</param>
        /// <returns></returns>
        [TestCase(ParameterType.HeadDiameter, 70.0, Result = 70.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.PistonHeight, 50.0, Result = 50.0,
            Description = "Установка параметра \"Высота поршня\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.BeltHeight, 2.0, Result = 2.0,
            Description = "Установка параметра \"Высота жарового пояса\" в пользовательском контроле в валидное значение")] 
        [TestCase(ParameterType.SkirtWidth, 30.0, Result = 30.0,
            Description = "Установка параметра \"Ширина юбки\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.SkirtHeight, 35.0, Result = 35.0,
            Description = "Установка параметра \"Высота юбки\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.RingDiameter, 15.0, Result = 15.0,
            Description = "Установка параметра \"Диаметр отверстия поршневого кольца\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.PartitionHeight, 2.0, Result = 2.0,
            Description = "Установка параметра \"Высота перегородки поршневых колец\" в пользовательском контроле в валидное значение")]
        [TestCase(ParameterType.HeadDiameter, 25.0, Result = 25.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в пользовательском контроле в минимальное значение")]
        [TestCase(ParameterType.PistonHeight, 24.0, Result = 24.0,
            Description = "Установка параметра \"Высота поршня\" в пользовательском контроле в минимальное значение")]
        [TestCase(ParameterType.BeltHeight, 1.8, Result = 1.8,
            Description = "Установка параметра \"Высота жарового пояса\" в пользовательском контроле в минимальное значение")]
        [TestCase(ParameterType.SkirtWidth, 15.0, Result = 25.0,
            Description = "Установка параметра \"Ширина юбки\" в пользовательском контроле в значение меньшее минимального")]
        [TestCase(ParameterType.SkirtHeight, 23.0, Result = 23.5,
            Description = "Установка параметра \"Высота юбки\" в пользовательском контроле в значение меньшее минимального")]
        [TestCase(ParameterType.RingDiameter, 13.0, Result = 13.0,
            Description = "Установка параметра \"Диаметр отверстия поршневого кольца\" в пользовательском контроле в минимальное значение")]
        [TestCase(ParameterType.PartitionHeight, 1.8, Result = 1.8,
            Description = "Установка параметра \"Высота перегородки поршневых колец\" в пользовательском контроле в минимальное значение")]
        [TestCase(ParameterType.HeadDiameter, 100.0, Result = 100.0,
            Description = "Установка параметра \"Диаметр головы поршня\" в пользовательском контроле в максимальное значение")]
        [TestCase(ParameterType.PistonHeight, 94.0, Result = 94.0,
            Description = "Установка параметра \"Высота поршня\" в пользовательском контроле в максимальное значение")]
        [TestCase(ParameterType.BeltHeight, 3.0, Result = 3.0,
          Description = "Установка параметра \"Высота жарового пояса\" в пользовательском контроле в максимальное значение")]
        [TestCase(ParameterType.SkirtWidth, 45.0, Result = 40.0,
            Description = "Установка параметра \"Ширина юбки\" в пользовательском контроле в значение большее максимального")]
        [TestCase(ParameterType.SkirtHeight, 40.0, Result = 37.6,
            Description = "Установка параметра \"Высота юбки\" в пользовательском контроле в значение большее максимального")]
        [TestCase(ParameterType.RingDiameter, 17.0, Result = 17.0,
            Description = "Установка параметра \"Диаметр отверстия поршневого кольца\" в пользовательском контроле в максимальное значение")]
        [TestCase(ParameterType.PartitionHeight, 3.8, Result = 3.0,
            Description = "Установка параметра \"Высота перегородки поршневых колец\" в пользовательском контроле в значение больше максимального")]
       public double SetParameter(ParameterType parameterType, double newValue)
        {
            //arrange
            var properties = new PistonProperties();

            //act
            properties.GetParameter(parameterType).Value = newValue;

            //assert
            return properties.GetParameter(parameterType).Value;
        }
    }
}
