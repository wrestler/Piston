﻿using System;
using System.Windows.Forms;

namespace Piston
{
    public partial class PistonForm : Form
    {
        /// <summary>
        /// Параметры модели
        /// </summary>
        private readonly PistonProperties _pistonProperties = new PistonProperties();

        /// <summary>
        /// Модель поршня
        /// </summary>
        private PistonModel _pistonModel;

        /// <summary>
        /// Интерфейс САПР API
        /// </summary>
        private InventorApi _inventorApi;

        public PistonForm()
        {
            InitializeComponent();
            InitParameters();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitParameters()
        {
            pistonHeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.PistonHeight);
            headDiameterUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.HeadDiameter);
            skirtHeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.SkirtHeight);
            skirtWidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.SkirtWidth);
            bossHeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.BossHeight);
            bossWidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.BossWidth);
            bossLengthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.BossLength);
            beltHeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.BeltHeight);
            ringDiameterUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RingDiameter);
            ring1WidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.Ring1Width);
            ring1HeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.Ring1Height);
            ring2WidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.Ring2Width);
            ring2HeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.Ring2Height);
            partitionHeightUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.PartitionHeight);
            rodLengthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodLength);
            rodDiameterUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodDiameter);
            rodHeadWidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodHeadWidth);
            rodHead1DiameterUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodHead1Diameter);
            rodHead1WidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodHead1Width);
            rodHead2DiameterUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodHead2Diameter);
            rodHead2WidthUpDown.Parameter = _pistonProperties.GetParameter(ParameterType.RodHead2Width);

        }

        /// <summary>
        /// Построение модели
        /// </summary>
        /// <param name="sender">Отправитель события</param>
        /// <param name="e">Параметры</param>
        private void buildPiston_Click(object sender, EventArgs e)
        {
            //_inventorApi = new InventorApi();
            //_pistonModel = new PistonModel(_pistonProperties, _inventorApi);
            //_pistonModel.Build();
            LoadTestBuild();

        }
        /// <summary>
        /// Нагрзочный тест с i-количеством повторений модели
        /// </summary>
        private void LoadTestBuild()
        {
            for (var i = 0; i < 250; i++)
            {
                var d1 = DateTime.Now;
                _inventorApi = new InventorApi();
                _pistonModel = new PistonModel(_pistonProperties, _inventorApi);
                _pistonModel.Build();
                var d2 = DateTime.Now;
                var diff = d2 - d1;

                var millisceonds = (int)diff.TotalMilliseconds;
                Console.WriteLine(millisceonds);
            }

        }

    }
}

