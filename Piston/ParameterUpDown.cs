﻿using System;
using System.Windows.Forms;

namespace Piston
{
    /// <summary>
    /// Контрол для параметра.
    /// </summary>
    public partial class ParameterUpDown : UserControl
    {
        /// <summary>
        /// Параметр, с которым работает контрол.
        /// </summary>
        private Parameter _parameter;

        /// <summary>
        /// Установлен ли параметр в контрол.
        /// </summary>
        private bool _inited;

        /// <summary>
        /// Приостановлена ли обработка изменения значений параметра.
        /// </summary>
        private bool _parameterChangedStopped;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public ParameterUpDown()
        {
            InitializeComponent();
            numericUpDown1.ValueChanged += NumericUpDown1OnValueChanged;
        }

        /// <summary>
        /// Обработчик событий изменения значений элемента <see cref="NumericUpDown"/>.
        /// </summary>
        /// <param name="sender">Объект-инициатор события.</param>
        /// <param name="eventArgs">Аргументы события.</param>
        private void NumericUpDown1OnValueChanged(object sender, EventArgs eventArgs)
        {
            if (!_inited)
                return;

            _parameterChangedStopped = true;

            _parameter.Min = Convert.ToDouble(numericUpDown1.Minimum);
            _parameter.Max = Convert.ToDouble(numericUpDown1.Maximum);
            _parameter.Value = Convert.ToDouble(numericUpDown1.Value);

            _parameterChangedStopped = false;
        }

        /// <summary>
        /// Параметр, с которым работает контрол.
        /// </summary>
        public Parameter Parameter
        {
            get { return _parameter; }
            set
            {
                if (_parameter == value)
                    return;
                if (_parameter != null)
                {
                    _parameter.ParameterChanged -= ParameterOnParameterChanged;
                }
                if (value != null)
                {
                    value.ParameterChanged += ParameterOnParameterChanged;
                    numericUpDown1.Minimum = Convert.ToDecimal(value.Min);
                    numericUpDown1.Maximum = Convert.ToDecimal(value.Max);
                    numericUpDown1.Value = Convert.ToDecimal(value.Value);
                    _inited = true;
                }
                else
                {
                    _inited = false;
                }
                _parameter = value;
            }
        }

        /// <summary>
        /// Обработчик событий изменения значений параметра, с которым работает контрол.
        /// </summary>
        /// <param name="sender">Объект-инициатор события.</param>
        /// <param name="eventArgs">Аргументы события.</param>
        private void ParameterOnParameterChanged(object sender, EventArgs eventArgs)
        {
            if (_parameterChangedStopped || !_inited)
                return;

            var parameter = (Parameter)sender;
            numericUpDown1.Minimum = Convert.ToDecimal(parameter.Min);
            numericUpDown1.Maximum = Convert.ToDecimal(parameter.Max);
            numericUpDown1.Value = Convert.ToDecimal(parameter.Value);
        }
    }
}
