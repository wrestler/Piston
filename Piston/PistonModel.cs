﻿using System;
using System.Collections.Generic;
using Inventor;

namespace Piston
{
    /// <summary>
    /// Класс инкапсулирует модель, вызывает построение модели
    /// </summary>
    public class PistonModel 
    {
        #region Fields

        /// <summary>
        /// Параметры модели
        /// </summary>
        private readonly PistonProperties _pistonProperties;

        /// <summary>
        /// САПР API
        /// </summary>
        private readonly InventorApi _api;

        #endregion

        #region Methods

        /// <summary>
        /// Конструктор с входными параметрами модели
        /// </summary>
        /// <param name="pistonProperties">Параметры модели</param>
        /// <param name="inventorApi"></param>
        public PistonModel(PistonProperties pistonProperties, InventorApi inventorApi)
        {
            _pistonProperties = pistonProperties;
            _api = inventorApi;
        }

        /// <summary>
        /// Функция строит модель
        /// </summary>
        public void Build()
        {
            BuildSkirt();
            BuildHead();
           // BuildRod();
            ChangeMaterial();
        }

        static void ChangeMaterial()
        {
            var partDocument = (PartDocument) InventorApi.InvApp.ActiveDocument;
            var materialsLibrary = partDocument.Materials;
            var myMaterial = materialsLibrary["Steel"];
            var tempMaterial = myMaterial.StyleLocation == StyleLocationEnum.kLibraryStyleLocation
            ? myMaterial.ConvertToLocal()
            : myMaterial;
            partDocument.ComponentDefinition.Material = tempMaterial;
            partDocument.Update();
        }
        
        /// <summary>
        /// Функция строит шатун
        /// </summary>
        private void BuildRod()
        {
            var headDiameter = _pistonProperties.GetParameter(ParameterType.HeadDiameter).Value;
            var bossHeight = _pistonProperties.GetParameter(ParameterType.BossHeight).Value;
            var bossWidth = _pistonProperties.GetParameter(ParameterType.BossWidth).Value;
            var rodLength = _pistonProperties.GetParameter(ParameterType.RodLength).Value;
            var rodDiameter = _pistonProperties.GetParameter(ParameterType.RodDiameter).Value;
            var rodHeadWidth = _pistonProperties.GetParameter(ParameterType.RodHeadWidth).Value;
            var rodHead1Diameter = _pistonProperties.GetParameter(ParameterType.RodHead1Diameter).Value;
            var rodHead1Width = _pistonProperties.GetParameter(ParameterType.RodHead1Width).Value;
            var rodHead2Diameter = _pistonProperties.GetParameter(ParameterType.RodHead2Diameter).Value;
            var rodHead2Width = _pistonProperties.GetParameter(ParameterType.RodHead2Width).Value;
            var ringDiameter = _pistonProperties.GetParameter(ParameterType.RingDiameter).Value;
            
            //Неразъемная головка
            _api.MakeNewSketch(1, -rodHeadWidth/2);
            _api.DrawCircle(0.0, bossHeight / 2, rodHead1Diameter + rodHead1Width);
            _api.DrawCircle(0.0, bossHeight / 2, rodHead1Diameter);
            _api.Extrude(rodHeadWidth);

            //Крепление
            var mount = rodHead1Diameter > ringDiameter ? ringDiameter : rodHead1Diameter;

            _api.MakeNewSketch(1, -headDiameter / 2 + bossWidth * 1.5);
            _api.DrawCircle(0.0, bossHeight / 2, mount);
            _api.Extrude(headDiameter - bossWidth*3);

            //Стержень
            _api.MakeNewSketch(3, -(bossHeight / 2 - rodHead1Diameter));
            _api.DrawCircle(0.0, 0.0, rodDiameter);
            _api.Extrude(rodLength - rodHead2Width / 1.9, PartFeatureExtentDirectionEnum.kNegativeExtentDirection);

            //разъемная головка
            _api.MakeNewSketch(1, -rodHeadWidth / 2);
            _api.DrawCircle(0.0, bossHeight / 2 - rodHead1Diameter - rodLength - rodHead1Width/2, rodHead2Diameter + rodHead2Width);
            _api.DrawCircle(0.0, bossHeight / 2 - rodHead1Diameter - rodLength - rodHead1Width/2, rodHead2Diameter);
            _api.Extrude(rodHeadWidth);

            //отрисовка треугольников
            const double scale = 0.42;
            var triangles = new List<Tuple<double, double, double, double>>
            {
                new Tuple<double, double, double, double>(rodDiameter / 2, -rodHeadWidth, 
                    -rodHeadWidth * scale, rodDiameter / 2),
                new Tuple<double, double, double, double>(-rodLength + rodHead2Diameter / 2 - rodHead1Diameter / 4, -rodLength + rodHeadWidth * 1.7,
                    -rodHeadWidth * scale, -rodLength + rodHead2Diameter / 2 - rodHead2Width / 4 - rodHead1Diameter / 4)
            };

            foreach (var triangle in triangles)
            {
                //для неразъемной головки
                _api.MakeNewSketch(2, 0.0);
                _api.DrawLine(0.0, triangle.Item1, 0.0, triangle.Item2);
                _api.DrawLine(triangle.Item3, triangle.Item4);
                _api.CloseShape();

                //вращение
                _api.RevolveFull(0.0, -1.0, 0.0, 1.0);
            }
        }

        /// <summary>
        /// Функция строит голову поршня
        /// </summary>
        private void BuildHead()
        {
            var headDiameter = _pistonProperties.GetParameter(ParameterType.HeadDiameter).Value;
            var ring1Height = _pistonProperties.GetParameter(ParameterType.Ring1Height).Value;
            var ring1Width = _pistonProperties.GetParameter(ParameterType.Ring1Width).Value;
            var ring2Height = _pistonProperties.GetParameter(ParameterType.Ring2Height).Value;
            var ring2Width = _pistonProperties.GetParameter(ParameterType.Ring2Width).Value;
            var partitionHeight = _pistonProperties.GetParameter(ParameterType.PartitionHeight).Value;
            var beltHeight = _pistonProperties.GetParameter(ParameterType.BeltHeight).Value;
            var skirtHeight = _pistonProperties.GetParameter(ParameterType.SkirtHeight).Value;

            //Отрисовывает голову
            var rings = new List<Tuple<double, double, double>>
            {
                new Tuple<double, double, double>(skirtHeight, headDiameter - ring1Width, ring1Height),
                new Tuple<double, double, double>(skirtHeight + ring1Height, headDiameter, partitionHeight),
                new Tuple<double, double, double>(skirtHeight + ring1Height + partitionHeight, headDiameter - ring2Width, ring2Height),
                new Tuple<double, double, double>(skirtHeight + ring1Height + partitionHeight + ring2Height, headDiameter, beltHeight)
            };

            foreach (var ring in rings)
            {
                _api.MakeNewSketch(3, ring.Item1);
                _api.DrawCircle(0.0, 0.0, ring.Item2);
                _api.Extrude(ring.Item3);
            }
        }

        /// <summary>
        /// Функция строит юбку поршня
        /// </summary>
        private void BuildSkirt()
        {
            var headDiameter = _pistonProperties.GetParameter(ParameterType.HeadDiameter).Value;
            var skirtHeight = _pistonProperties.GetParameter(ParameterType.SkirtHeight).Value;
            var skirtWidth = _pistonProperties.GetParameter(ParameterType.SkirtWidth).Value;
            var bossHeight = _pistonProperties.GetParameter(ParameterType.BossHeight).Value;
            var bossWidth = _pistonProperties.GetParameter(ParameterType.BossWidth).Value;
            var bossLength = _pistonProperties.GetParameter(ParameterType.BossLength).Value;
            var ringDiameter = _pistonProperties.GetParameter(ParameterType.RingDiameter).Value;

            //тело юбки
            _api.MakeNewSketch(3, 0);
            _api.DrawCircle(0.0, 0.0, headDiameter);
            _api.Extrude(skirtHeight);
           
            //бабышки
            _api.MakeNewSketch(3, 0);
            //_api.CutExtrudeRectangle(25, 15 ,22, -15, bossHeight);
            //_api.CutExtrudeRectangle(-25, 15, -22, -15, bossHeight);
            _api.CutExtrudeRectangle(headDiameter / 2, bossLength / 2, headDiameter / 2 - bossWidth, -bossLength / 2, bossHeight);
            _api.CutExtrudeRectangle(-headDiameter / 2, bossLength / 2, -headDiameter / 2 + bossWidth, -bossLength / 2, bossHeight);
           
            // отверстие под шатун
            _api.MakeNewSketch(3, 0);
            _api.CutExtrudeCircle(0.0, 0.0, skirtWidth, skirtHeight);

            //отверстие под крепление шатуна
            _api.MakeNewSketch(1, -headDiameter / 2);
            _api.CutExtrudeCircle(0.0, bossHeight / 2, ringDiameter, headDiameter);    
        }

        #endregion
    }
}
