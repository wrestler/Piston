﻿namespace Piston
{
    partial class PistonForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.buildPiston = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.rodHeadWidthUpDown = new Piston.ParameterUpDown();
            this.rodDiameterUpDown = new Piston.ParameterUpDown();
            this.rodHead2WidthUpDown = new Piston.ParameterUpDown();
            this.rodHead2DiameterUpDown = new Piston.ParameterUpDown();
            this.rodHead1WidthUpDown = new Piston.ParameterUpDown();
            this.rodHead1DiameterUpDown = new Piston.ParameterUpDown();
            this.rodLengthUpDown = new Piston.ParameterUpDown();
            this.partitionHeightUpDown = new Piston.ParameterUpDown();
            this.ring2HeightUpDown = new Piston.ParameterUpDown();
            this.ring2WidthUpDown = new Piston.ParameterUpDown();
            this.ring1WidthUpDown = new Piston.ParameterUpDown();
            this.ring1HeightUpDown = new Piston.ParameterUpDown();
            this.ringDiameterUpDown = new Piston.ParameterUpDown();
            this.beltHeightUpDown = new Piston.ParameterUpDown();
            this.bossLengthUpDown = new Piston.ParameterUpDown();
            this.bossWidthUpDown = new Piston.ParameterUpDown();
            this.bossHeightUpDown = new Piston.ParameterUpDown();
            this.skirtWidthUpDown = new Piston.ParameterUpDown();
            this.skirtHeightUpDown = new Piston.ParameterUpDown();
            this.headDiameterUpDown = new Piston.ParameterUpDown();
            this.pistonHeightUpDown = new Piston.ParameterUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(206, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Поршень ДВС";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Высота поршня";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Диаметр головы поршня";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.headDiameterUpDown);
            this.groupBox1.Controls.Add(this.pistonHeightUpDown);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(6, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 72);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Размеры поршня";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Высота юбки поршня";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Ширина юбки поршня";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.skirtWidthUpDown);
            this.groupBox2.Controls.Add(this.skirtHeightUpDown);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(6, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(253, 72);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Размеры юбки поршня";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Высота бобышки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Ширина бобышки";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Толщина бобышки";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bossLengthUpDown);
            this.groupBox3.Controls.Add(this.bossWidthUpDown);
            this.groupBox3.Controls.Add(this.bossHeightUpDown);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(6, 201);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(253, 98);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Размеры бобышки";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Высота жарового пояса";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.beltHeightUpDown);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(308, 305);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(307, 46);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Размеры жарового пояса";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(211, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Диаметр отверстия поршневого кольца";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ringDiameterUpDown);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(266, 45);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(349, 45);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Размеры отверстия поршневого кольца";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(222, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Толщина канавки маслосъемного кольца";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(214, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Высота канавки маслосъемного кольца";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ring1WidthUpDown);
            this.groupBox6.Controls.Add(this.ring1HeightUpDown);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Location = new System.Drawing.Point(266, 96);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(349, 71);
            this.groupBox6.TabIndex = 23;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Размеры канавки маслосъемного кольца";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ring2HeightUpDown);
            this.groupBox7.Controls.Add(this.ring2WidthUpDown);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Location = new System.Drawing.Point(266, 173);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(349, 71);
            this.groupBox7.TabIndex = 24;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Размеры канавки компрессионного кольца";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(223, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Высота канавки компрессионного кольца";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(231, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Толщина канавки компрессионного кольца";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(206, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "Высота перегородки поршневых колец";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.partitionHeightUpDown);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Location = new System.Drawing.Point(266, 250);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(349, 47);
            this.groupBox8.TabIndex = 25;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Размеры перегородки";
            // 
            // buildPiston
            // 
            this.buildPiston.Location = new System.Drawing.Point(391, 398);
            this.buildPiston.Name = "buildPiston";
            this.buildPiston.Size = new System.Drawing.Size(170, 76);
            this.buildPiston.TabIndex = 26;
            this.buildPiston.Text = "Построить";
            this.buildPiston.UseVisualStyleBackColor = true;
            this.buildPiston.Click += new System.EventHandler(this.buildPiston_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 13);
            this.label16.TabIndex = 27;
            this.label16.Text = "Длина стержня";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 104);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(169, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Диаметр неразъемной головки";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 156);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(157, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "Диаметр разъемной головки";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 130);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(168, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Толшина неразъемной головки";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(157, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "Толщина разъемной головки";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rodHeadWidthUpDown);
            this.groupBox9.Controls.Add(this.rodDiameterUpDown);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.rodHead2WidthUpDown);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.rodHead2DiameterUpDown);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.rodHead1WidthUpDown);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.rodHead1DiameterUpDown);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.rodLengthUpDown);
            this.groupBox9.Location = new System.Drawing.Point(6, 305);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(296, 202);
            this.groupBox9.TabIndex = 37;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Размеры шатуна";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(90, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "Ширина головок";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 13);
            this.label21.TabIndex = 37;
            this.label21.Text = "Диаметр стержня";
            // 
            // rodHeadWidthUpDown
            // 
            this.rodHeadWidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodHeadWidthUpDown.Location = new System.Drawing.Point(180, 71);
            this.rodHeadWidthUpDown.Name = "rodHeadWidthUpDown";
            this.rodHeadWidthUpDown.Parameter = null;
            this.rodHeadWidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodHeadWidthUpDown.TabIndex = 40;
            // 
            // rodDiameterUpDown
            // 
            this.rodDiameterUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodDiameterUpDown.Location = new System.Drawing.Point(181, 45);
            this.rodDiameterUpDown.Name = "rodDiameterUpDown";
            this.rodDiameterUpDown.Parameter = null;
            this.rodDiameterUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodDiameterUpDown.TabIndex = 39;
            // 
            // rodHead2WidthUpDown
            // 
            this.rodHead2WidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodHead2WidthUpDown.Location = new System.Drawing.Point(180, 175);
            this.rodHead2WidthUpDown.Name = "rodHead2WidthUpDown";
            this.rodHead2WidthUpDown.Parameter = null;
            this.rodHead2WidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodHead2WidthUpDown.TabIndex = 36;
            // 
            // rodHead2DiameterUpDown
            // 
            this.rodHead2DiameterUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodHead2DiameterUpDown.Location = new System.Drawing.Point(180, 149);
            this.rodHead2DiameterUpDown.Name = "rodHead2DiameterUpDown";
            this.rodHead2DiameterUpDown.Parameter = null;
            this.rodHead2DiameterUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodHead2DiameterUpDown.TabIndex = 35;
            // 
            // rodHead1WidthUpDown
            // 
            this.rodHead1WidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodHead1WidthUpDown.Location = new System.Drawing.Point(180, 123);
            this.rodHead1WidthUpDown.Name = "rodHead1WidthUpDown";
            this.rodHead1WidthUpDown.Parameter = null;
            this.rodHead1WidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodHead1WidthUpDown.TabIndex = 34;
            // 
            // rodHead1DiameterUpDown
            // 
            this.rodHead1DiameterUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodHead1DiameterUpDown.Location = new System.Drawing.Point(180, 97);
            this.rodHead1DiameterUpDown.Name = "rodHead1DiameterUpDown";
            this.rodHead1DiameterUpDown.Parameter = null;
            this.rodHead1DiameterUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodHead1DiameterUpDown.TabIndex = 33;
            // 
            // rodLengthUpDown
            // 
            this.rodLengthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rodLengthUpDown.Location = new System.Drawing.Point(181, 19);
            this.rodLengthUpDown.Name = "rodLengthUpDown";
            this.rodLengthUpDown.Parameter = null;
            this.rodLengthUpDown.Size = new System.Drawing.Size(106, 20);
            this.rodLengthUpDown.TabIndex = 32;
            // 
            // partitionHeightUpDown
            // 
            this.partitionHeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.partitionHeightUpDown.Location = new System.Drawing.Point(237, 15);
            this.partitionHeightUpDown.Name = "partitionHeightUpDown";
            this.partitionHeightUpDown.Parameter = null;
            this.partitionHeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.partitionHeightUpDown.TabIndex = 24;
            // 
            // ring2HeightUpDown
            // 
            this.ring2HeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ring2HeightUpDown.Location = new System.Drawing.Point(237, 43);
            this.ring2HeightUpDown.Name = "ring2HeightUpDown";
            this.ring2HeightUpDown.Parameter = null;
            this.ring2HeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.ring2HeightUpDown.TabIndex = 23;
            // 
            // ring2WidthUpDown
            // 
            this.ring2WidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ring2WidthUpDown.Location = new System.Drawing.Point(237, 14);
            this.ring2WidthUpDown.Name = "ring2WidthUpDown";
            this.ring2WidthUpDown.Parameter = null;
            this.ring2WidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.ring2WidthUpDown.TabIndex = 22;
            // 
            // ring1WidthUpDown
            // 
            this.ring1WidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ring1WidthUpDown.Location = new System.Drawing.Point(237, 14);
            this.ring1WidthUpDown.Name = "ring1WidthUpDown";
            this.ring1WidthUpDown.Parameter = null;
            this.ring1WidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.ring1WidthUpDown.TabIndex = 23;
            // 
            // ring1HeightUpDown
            // 
            this.ring1HeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ring1HeightUpDown.Location = new System.Drawing.Point(237, 40);
            this.ring1HeightUpDown.Name = "ring1HeightUpDown";
            this.ring1HeightUpDown.Parameter = null;
            this.ring1HeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.ring1HeightUpDown.TabIndex = 22;
            // 
            // ringDiameterUpDown
            // 
            this.ringDiameterUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ringDiameterUpDown.Location = new System.Drawing.Point(237, 15);
            this.ringDiameterUpDown.Name = "ringDiameterUpDown";
            this.ringDiameterUpDown.Parameter = null;
            this.ringDiameterUpDown.Size = new System.Drawing.Size(106, 20);
            this.ringDiameterUpDown.TabIndex = 18;
            // 
            // beltHeightUpDown
            // 
            this.beltHeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.beltHeightUpDown.Location = new System.Drawing.Point(195, 15);
            this.beltHeightUpDown.Name = "beltHeightUpDown";
            this.beltHeightUpDown.Parameter = null;
            this.beltHeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.beltHeightUpDown.TabIndex = 16;
            // 
            // bossLengthUpDown
            // 
            this.bossLengthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bossLengthUpDown.Location = new System.Drawing.Point(141, 67);
            this.bossLengthUpDown.Name = "bossLengthUpDown";
            this.bossLengthUpDown.Parameter = null;
            this.bossLengthUpDown.Size = new System.Drawing.Size(106, 20);
            this.bossLengthUpDown.TabIndex = 16;
            // 
            // bossWidthUpDown
            // 
            this.bossWidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bossWidthUpDown.Location = new System.Drawing.Point(141, 41);
            this.bossWidthUpDown.Name = "bossWidthUpDown";
            this.bossWidthUpDown.Parameter = null;
            this.bossWidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.bossWidthUpDown.TabIndex = 15;
            // 
            // bossHeightUpDown
            // 
            this.bossHeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bossHeightUpDown.Cursor = System.Windows.Forms.Cursors.Default;
            this.bossHeightUpDown.Location = new System.Drawing.Point(141, 15);
            this.bossHeightUpDown.Name = "bossHeightUpDown";
            this.bossHeightUpDown.Parameter = null;
            this.bossHeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.bossHeightUpDown.TabIndex = 14;
            // 
            // skirtWidthUpDown
            // 
            this.skirtWidthUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.skirtWidthUpDown.Location = new System.Drawing.Point(142, 41);
            this.skirtWidthUpDown.Name = "skirtWidthUpDown";
            this.skirtWidthUpDown.Parameter = null;
            this.skirtWidthUpDown.Size = new System.Drawing.Size(106, 20);
            this.skirtWidthUpDown.TabIndex = 9;
            // 
            // skirtHeightUpDown
            // 
            this.skirtHeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.skirtHeightUpDown.Location = new System.Drawing.Point(142, 15);
            this.skirtHeightUpDown.Name = "skirtHeightUpDown";
            this.skirtHeightUpDown.Parameter = null;
            this.skirtHeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.skirtHeightUpDown.TabIndex = 8;
            // 
            // headDiameterUpDown
            // 
            this.headDiameterUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.headDiameterUpDown.Location = new System.Drawing.Point(142, 41);
            this.headDiameterUpDown.Name = "headDiameterUpDown";
            this.headDiameterUpDown.Parameter = null;
            this.headDiameterUpDown.Size = new System.Drawing.Size(106, 20);
            this.headDiameterUpDown.TabIndex = 5;
            // 
            // pistonHeightUpDown
            // 
            this.pistonHeightUpDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pistonHeightUpDown.Location = new System.Drawing.Point(142, 15);
            this.pistonHeightUpDown.Name = "pistonHeightUpDown";
            this.pistonHeightUpDown.Parameter = null;
            this.pistonHeightUpDown.Size = new System.Drawing.Size(106, 20);
            this.pistonHeightUpDown.TabIndex = 4;
            // 
            // PistonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 513);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.buildPiston);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "PistonForm";
            this.Text = "Построение поршня ДВС";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button buildPiston;
        private ParameterUpDown pistonHeightUpDown;
        private ParameterUpDown headDiameterUpDown;
        private ParameterUpDown skirtHeightUpDown;
        private ParameterUpDown skirtWidthUpDown;
        private ParameterUpDown bossHeightUpDown;
        private ParameterUpDown bossWidthUpDown;
        private ParameterUpDown bossLengthUpDown;
        private ParameterUpDown beltHeightUpDown;
        private ParameterUpDown ringDiameterUpDown;
        private ParameterUpDown ring1HeightUpDown;
        private ParameterUpDown ring1WidthUpDown;
        private ParameterUpDown ring2WidthUpDown;
        private ParameterUpDown ring2HeightUpDown;
        private ParameterUpDown partitionHeightUpDown;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private ParameterUpDown rodLengthUpDown;
        private ParameterUpDown rodHead1DiameterUpDown;
        private ParameterUpDown rodHead1WidthUpDown;
        private ParameterUpDown rodHead2DiameterUpDown;
        private ParameterUpDown rodHead2WidthUpDown;
        private System.Windows.Forms.GroupBox groupBox9;
        private ParameterUpDown rodHeadWidthUpDown;
        private ParameterUpDown rodDiameterUpDown;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;

    }
}

