﻿namespace Piston
{
    /// <summary>
    /// Тип параметра модели
    /// </summary>
    public enum ParameterType
    {
        /// <summary>
        /// Высота жарового пояса (1)
        /// </summary>
        BeltHeight,

        /// <summary>
        /// Высота бобышки (2)
        /// </summary>
        BossHeight,

        /// <summary>
        /// Ширина бобышки (3)
        /// </summary>
        BossLength,

        /// <summary>
        /// Толщина бобышки (4)
        /// </summary>
        BossWidth,

        /// <summary>
        /// Диаметр головы поршня (5)
        /// </summary>
        HeadDiameter,

        /// <summary>
        /// Высота перегородки поршневых колец(6)
        /// </summary>
        PartitionHeight,

        /// <summary>
        /// Высота поршня (7)
        /// </summary>
        PistonHeight,

        /// <summary>
        ///  Высота канавки маслосъемного кольца (8)
        /// </summary>
        Ring1Height,

        /// <summary>
        /// Ширина канавки маслосъемного кольца (9)
        /// </summary>
        Ring1Width,

        /// <summary>
        /// Высота канавки компрессионного кольца (10)
        /// </summary>
        Ring2Height,

        /// <summary>
        /// Ширина канавки компрессионного кольца (11)
        /// </summary>
        Ring2Width,

        /// <summary>
        /// Диаметр отверстия поршневого кольца (12)
        /// </summary>
        RingDiameter,

        /// <summary>
        /// Высота юбки поршня (13)
        /// </summary>
        SkirtHeight,

        /// <summary>
        /// Ширина юбки поршня (14)
        /// </summary>
        SkirtWidth,
        
        /// <summary>
        /// Длина стержня (15)
        /// </summary>
        RodLength,

        /// <summary>
        /// Диаметер стержня (16)
        /// </summary>
        RodDiameter,

        /// <summary>
        /// Ширина головок (17)
        /// </summary>
        RodHeadWidth,

        /// <summary>
        /// Диаметер неразъемной головки (18)
        /// </summary>
        RodHead1Diameter,
        
        /// <summary>
        /// Толщина неразъемной головки (19)
        /// </summary>
        RodHead1Width,

        /// <summary>
        /// Диаметер разъемной головки (20)
        /// </summary>
        RodHead2Diameter,

        /// <summary>
        /// Толщина разъемной головки (21)
        /// </summary>
        RodHead2Width
    }
}
