﻿using System;
using System.Collections.Generic;

namespace Piston
{
    /// <summary>
    /// Параметры модели
    /// </summary>
    public class PistonProperties
    {
        /// <summary>
        /// Словарь параметров поршня
        /// </summary>
        private static Dictionary<ParameterType, Parameter> _parameters;

        #region Methods

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public PistonProperties()
        {
            _parameters = new Dictionary<ParameterType, Parameter>
            {
                {ParameterType.BeltHeight, new Parameter(1.8, 3.0, 3.0)},
                {ParameterType.BossHeight, new Parameter(21.0, 25.0, 28.0)},
                {ParameterType.BossLength, new Parameter(15.0, 30.0, 50.0)},
                {ParameterType.BossWidth, new Parameter(1.0, 3.0, 4.0)},
                {ParameterType.HeadDiameter, new Parameter(25.0, 50.0, 100.0)},
                {ParameterType.PartitionHeight, new Parameter(1.8, 3.0, 3.0)},
                {ParameterType.PistonHeight, new Parameter(23.5, 47.0, 94.0)},
                {ParameterType.Ring1Height, new Parameter(1.8, 3.0, 3.0)},
                {ParameterType.Ring1Width, new Parameter(10.0, 5.0, 25.0)},
                {ParameterType.Ring2Height, new Parameter(1.8, 3.0, 3.0)},
                {ParameterType.Ring2Width, new Parameter(10.0, 5.0, 25.0)},
                {ParameterType.RingDiameter, new Parameter(13.0, 15.0, 18.0)},
                {ParameterType.SkirtHeight, new Parameter(23.5, 35.0, 37.6)},
                {ParameterType.SkirtWidth, new Parameter(25.0, 40.0, 40.0)},
                {ParameterType.RodLength, new Parameter(34.0, 35.0, 500.0)},
                {ParameterType.RodDiameter, new Parameter(6.6, 10.0, 13.3)},
                {ParameterType.RodHeadWidth, new Parameter(1.0, 15.0, 20.0)},
                {ParameterType.RodHead1Diameter, new Parameter(13.5, 15.0, 16.5)},
                {ParameterType.RodHead1Width, new Parameter(2.0, 5.0, 4.0)},
                {ParameterType.RodHead2Diameter, new Parameter(10.0, 15.0, 20.0)},
                {ParameterType.RodHead2Width, new Parameter(2.0, 5.0, 4.0)}
            };

            foreach (var parameter in _parameters.Values)
            {
                parameter.ParameterChanged += ParameterValueChanged;
            }
        }

        /// <summary>
        /// Обработчик, вызываемый при изменении какого-либо параметра модели
        /// </summary>
        /// <param name="sender">Отправитель</param>
        /// <param name="e">Аргументы</param>
        private void ParameterValueChanged(object sender, EventArgs e)
        {
            SetParameters();
        }

        /// <summary>
        /// Получить параметр
        /// </summary>
        /// <param name="parameterType">Тип параметра</param>
        /// <returns>Полученный параметр</returns>
        public Parameter GetParameter(ParameterType parameterType)
        {
            return _parameters[parameterType];
        }

        /// <summary>
        /// Пересчет границ параметра, зависящего от двух других параметров
        /// </summary>
        /// <param name="recaluculatedParameterType">Границы этой переменной изменяться</param>
        /// <param name="dependent1ParameterType">Первая переменная в зависимости</param>
        /// <param name="dependent2ParameterType">Вторая переменная в зависимости</param>
        /// <param name="ratioMax">Максимальный коэффицент соотношения</param>
        /// <param name="ratioMin">Минимальный коэффицент соотношения</param>
        private static void SetNewParameter2Dependents(ParameterType recaluculatedParameterType, ParameterType dependent1ParameterType, ParameterType dependent2ParameterType, double ratioMax, double ratioMin)
        {
            _parameters[recaluculatedParameterType].Max = (_parameters[dependent1ParameterType].Value - _parameters[dependent2ParameterType].Value) * ratioMax;
            _parameters[recaluculatedParameterType].Min = (_parameters[dependent1ParameterType].Value - _parameters[dependent2ParameterType].Value) * ratioMin;
            _parameters[recaluculatedParameterType].Validate();
        }
        /// <summary>
        /// Пересчет границ параметра, зависящего от одного параметра
        /// </summary>
        /// <param name="recaluculatedParameterType">Границы этой переменной изменяться</param>
        /// <param name="dependentParameterType">Переменная, от которой зависят</param>
        /// <param name="ratioMax">Максимальный коэффицент соотношения</param>
        /// <param name="ratioMin">Минимальный коэффицент соотношения</param>
        private static void SetNewParameter(ParameterType recaluculatedParameterType, ParameterType dependentParameterType, double ratioMax, double ratioMin)
        {
            _parameters[recaluculatedParameterType].Max = _parameters[dependentParameterType].Value * ratioMax;
            _parameters[recaluculatedParameterType].Min = _parameters[dependentParameterType].Value * ratioMin;
            _parameters[recaluculatedParameterType].Validate();
        }
       
        /// <summary>
        /// Проверка значение и установка ограничений
        /// </summary>
        private static void SetParameters()
        {
            //отверстие поршневого кольца
            const double ringDiameterValidateMax = 0.7;
            const double ringDiameterValidateMin = 0.5;
            var tempMax = _parameters[ParameterType.BossHeight].Value > (_parameters[ParameterType.BossLength].Value / 2) 
                ? _parameters[ParameterType.BossHeight].Value 
                : _parameters[ParameterType.BossLength].Value;
            _parameters[ParameterType.RingDiameter].Max = tempMax * ringDiameterValidateMax;
            _parameters[ParameterType.RingDiameter].Min = tempMax * ringDiameterValidateMin;
            _parameters[ParameterType.RingDiameter].Validate();

            //константы
            #region Piston
            //кольцо маслосъемное
              //высота
              const double ring1HeightValidateMax = 0.3;
              const double ring1HeightValidateMin = 0.15;
             
            //кольцо компрессионное
              //высота
              const double ring2HeightValidateMax = 0.3;
              const double ring2HeightValidateMin = 0.15;
            
            //перегородка
                //высота
                const double partitionHeightValidateMax = 0.25;
                const double partitionHeightValidateMin = 0.15;
            //жаровой пояс
                //высота
                const double beltHeightValidateMax = 0.25;
                const double beltHeightValidateMin = 0.15;
            #endregion
            
            //вычисление новых значений
            #region Piston
                var dependents = new List<Tuple<ParameterType, ParameterType, ParameterType, double, double>>
            {
             //кольцо маслосъемное
                //высота
                new Tuple <ParameterType, ParameterType, ParameterType, double, double>
                    (ParameterType.Ring1Height, ParameterType.PistonHeight, ParameterType.SkirtHeight, ring1HeightValidateMax, ring1HeightValidateMin),

             //кольцо компрессионное
                //высота
                new Tuple <ParameterType, ParameterType, ParameterType, double, double>
                    (ParameterType.Ring2Height, ParameterType.PistonHeight, ParameterType.SkirtHeight, ring2HeightValidateMax, ring2HeightValidateMin),
             //перегородка
                //высота
                new Tuple <ParameterType, ParameterType, ParameterType, double, double>
                    (ParameterType.PartitionHeight, ParameterType.PistonHeight, ParameterType.SkirtHeight, partitionHeightValidateMax, partitionHeightValidateMin),
            //жаровой пояс
                //высота
                new Tuple <ParameterType, ParameterType, ParameterType, double, double>
                    (ParameterType.BeltHeight, ParameterType.PistonHeight, ParameterType.SkirtHeight, beltHeightValidateMax, beltHeightValidateMin),
            };
                #endregion
            
            foreach (var dependent in dependents)
            {
                SetNewParameter2Dependents(dependent.Item1, dependent.Item2, dependent.Item3, dependent.Item4, dependent.Item5);
            }
            //константы
            #region Piston
        //поршень
            // юбка поршня
            const double skirtValidateMax = 0.8;
            const double skirtValidateMin = 0.5;
            
            //бобышка
                //высота
                const double bossHeightValidateMax = 0.8;
                const double bossHeightValidateMin = 0.6;
                //толщина
                const double bossLengthtValidateMin = 0.5;
            
           //кольцо маслосъемное
                //толщина
                const double ring1WidthValidateMax = 0.5;
                const double ring1WidthValidateMin = 0.1;
                
            //кольцо компрессионное
                //толщина
                const double ring2WidthValidateMax = 0.5;
                const double ring2WidthValidateMin = 0.1;
        #endregion
        
            #region Rod
        //шатун
            //стержень   
                //диаметр
                const double rodDiameterValidateMax = 0.3;
                const double rodDiameterValidateMin = 1.0 / 6.0;
                //длина
                const double rodLengthValidateMin = 1.1;

            //ширина головки
            const double rodHeadWidthValidateMax = 0.5;
            const double rodHeadWidthValidateMin = 1.0/6.0;

            //диаметер неразъемной головки
            const double rodHead1DiameterMaxValidate = 1.1;
            const double rodHead1DiameterMinValidate = 0.9;

            //толщина неразъемной головки
            const double rodHead1WidthValidateMax = 0.6;
            const double rodHead1WidthValidateMin = 0.4;

            //толщина разъемной головки
            const double rodHead2WidthValidateMax = 0.8;
            const double rodHead2WidthValidateMin = 0.4;
        #endregion
           
            //вычисление новых значений
            var parameters = new List<Tuple<ParameterType, ParameterType, double, double>>
            {
            #region Piston
            //поршень
                // юбка поршня
                    //высота юбки
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.SkirtHeight, ParameterType.PistonHeight, skirtValidateMax, skirtValidateMin),
                    //ширина юбки
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.SkirtWidth, ParameterType.HeadDiameter, skirtValidateMax, skirtValidateMin),
                //бобышка
                    //высота
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.BossHeight, ParameterType.SkirtHeight, bossHeightValidateMax, bossHeightValidateMin),
                    ////ширина
                    //new Tuple <ParameterType, ParameterType, double, double>
                    //    (ParameterType.BossWidth, ParameterType.HeadDiameter, bossWidthtValidateMax, bossWidthtValidateMin),
                    //толщина
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.BossLength, ParameterType.HeadDiameter, 1, bossLengthtValidateMin),
                
                //кольцо маслосъемное
                    //толщина
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.Ring1Width, ParameterType.HeadDiameter, ring1WidthValidateMax, ring1WidthValidateMin),
                    ////высота
                    //new Tuple <ParameterType, ParameterType, double, double>
                    //    (ParameterType.Ring1Height, ParameterType.PistonHeight, ring1HeightValidateMax, ring1HeightValidateMin),

                //кольцо компрессионное
                    //толщина
                    new Tuple <ParameterType, ParameterType, double, double>
                        (ParameterType.Ring2Width, ParameterType.HeadDiameter, ring2WidthValidateMax, ring2WidthValidateMin),

            #endregion
            
            #region Rod 
            //шатун
                //стержень   
                //диаметр
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodDiameter, ParameterType.SkirtWidth, rodDiameterValidateMax, rodDiameterValidateMin),
                //длина
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodLength, ParameterType.SkirtHeight, 4, rodLengthValidateMin),

                //ширина головки
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodHeadWidth, ParameterType.SkirtWidth, rodHeadWidthValidateMax, rodHeadWidthValidateMin),
                
                //диаметер неразъемной головки
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodHead1Diameter, ParameterType.RingDiameter, rodHead1DiameterMaxValidate, rodHead1DiameterMinValidate),

                //толщина неразъемной головки
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodHead1Width, ParameterType.RodHead1Diameter, rodHead1WidthValidateMax, rodHead1WidthValidateMin),

                //толщина разъемной головки
                new Tuple <ParameterType, ParameterType, double, double>
                    (ParameterType.RodHead2Width, ParameterType.RodHead2Diameter, rodHead2WidthValidateMax, rodHead2WidthValidateMin)
                #endregion
            };
            
            foreach (var parameter in parameters)
            {
                SetNewParameter(parameter.Item1, parameter.Item2, parameter.Item3, parameter.Item4);
            }
            
        }
        #endregion
    }
}
